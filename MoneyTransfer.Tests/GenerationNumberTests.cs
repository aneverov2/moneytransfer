﻿
using System.Linq;
using MoneyTransfer.Models;
using Xunit;

namespace MoneyTransfer.Tests
{
    
    public class GenerationNumberTests
    {
        public ApplicationContext _context { get; set; }

        public GenerationNumberTests(ApplicationContext context)
        {
            _context = context;
        }
        [Fact]
        public void NumberGeneratorTest()
        {
            AccountUniqueNumberGenerator generator = new AccountUniqueNumberGenerator(_context);
            bool result = generator.UniqueNumberCheck();
            Assert.True(result);
        }
    }

    public class ExistionAccountTests
    {
        public ApplicationContext _context { get; set; }

        public ExistionAccountTests(ApplicationContext context)
        {
            _context = context;
        }
        [Fact]
        public void ExistionTest()
        {
            int account = 123456;
            Account acc = _context.Accounts.FirstOrDefault(a => a.AccountNumber == account);
            Assert.NotNull(acc);
        }
    }
}
