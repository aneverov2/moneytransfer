﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoneyTransfer.Models;
using MoneyTransfer.ViewModels;

namespace MoneyTransfer.Controllers
{
    public class UserController : Controller
    {
        private ApplicationContext _context { get; set; }
        private readonly UserManager<User> _userManager;

        public UserController(
            ApplicationContext context,
            UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: User
        public ActionResult Index()
        {
            User user = _context.Users.Include(u => u.Account).FirstOrDefault(u => u.Email == HttpContext.User.Identity.Name);
            UserViewModel model = new UserViewModel()
            {
                User = user
            };
            return View(model);
        }

        public async Task<ActionResult> AddToBalance(int accountNumber, int summ)
        {
            Account account = _context.Accounts.FirstOrDefault(a => a.AccountNumber == accountNumber);
            account.AccountBalance += summ;

            await _context.SaveChangesAsync();
            return View();
        }

        public async Task<ActionResult> Transaction(int accountNumber, int summ)
        {
            User userFrom = _context.Users.Include(u => u.Account).FirstOrDefault(u => u.Email == HttpContext.User.Identity.Name);
            User userTo = _context.Users.Include(u => u.Account).FirstOrDefault(u => u.AccountNumber == accountNumber);
            if (userFrom.Account.AccountBalance < summ)
            {
                return View();
            }
            _context.Transactions.Add(new Transaction()
            {
                Summ = summ,
                TransactionDate = DateTime.Now,
                UserFormId = userFrom.Id,
                UserToId = userTo.Id
            });
            userFrom.Account.AccountBalance -= summ;
            userTo.Account.AccountBalance += summ;
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }


        // GET: User/Details/5
        public ActionResult Details()
        {
            User user = _context.Users
                .Include(u => u.Account)
                .FirstOrDefault(u => u.Email == HttpContext.User.Identity.Name);
            List<Transaction> transactions = _context.Transactions
                .Include(t => t.UserTo)
                .Include(t => t.UserForm)
                .Where(u => u.UserFormId == user.Id || u.UserToId == user.Id).ToList();
            TransactionViewModel model = new TransactionViewModel()
            {
                User = user,
                Transactions = transactions
            };
            return View(model);
        }

        // GET: User/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: User/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: User/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: User/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}