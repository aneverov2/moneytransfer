﻿
using System.Collections.Generic;
using MoneyTransfer.Models;

namespace MoneyTransfer.ViewModels
{
    public class TransactionViewModel
    {
        public User User { get; set; }
        public List<Transaction> Transactions { get; set; }
        public Account Account { get; set; }
    }
}
