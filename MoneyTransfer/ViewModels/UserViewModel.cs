﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoneyTransfer.Models;

namespace MoneyTransfer.ViewModels
{
    public class UserViewModel
    {
        public User User { get; set; }  
        public Account Account { get; set; }  
    }
}
