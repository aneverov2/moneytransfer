﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace MoneyTransfer.Migrations
{
    public partial class ModTransaction_5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_AspNetUsers_UserFromId",
                table: "Transactions");

            migrationBuilder.DropIndex(
                name: "IX_Transactions_UserFromId",
                table: "Transactions");

            migrationBuilder.DropColumn(
                name: "UserFromId",
                table: "Transactions");

            migrationBuilder.AlterColumn<string>(
                name: "UserFormId",
                table: "Transactions",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_UserFormId",
                table: "Transactions",
                column: "UserFormId");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_AspNetUsers_UserFormId",
                table: "Transactions",
                column: "UserFormId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_AspNetUsers_UserFormId",
                table: "Transactions");

            migrationBuilder.DropIndex(
                name: "IX_Transactions_UserFormId",
                table: "Transactions");

            migrationBuilder.AlterColumn<string>(
                name: "UserFormId",
                table: "Transactions",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserFromId",
                table: "Transactions",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_UserFromId",
                table: "Transactions",
                column: "UserFromId");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_AspNetUsers_UserFromId",
                table: "Transactions",
                column: "UserFromId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
