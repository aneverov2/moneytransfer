﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.Models
{
    public class Account
    {
        public int Id { get; set; }
        public int AccountNumber { get; set; }  
        public int AccountBalance { get; set; }

    }
}
