﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace MoneyTransfer.Models
{
    public class AccountUniqueNumberGenerator
    {
        public int Id { get; set; }
        public int AccountNumber { get; set; }
        public Random Rnd { get; set; }
        public ApplicationContext _context { get; set; }

        public AccountUniqueNumberGenerator(ApplicationContext context)
        {
            _context = context;
            Rnd = new Random();
            AccountNumber = Rnd.Next(1, 999999);
        }

        public bool UniqueNumberCheck()
        {
            User user = _context.Users.Include(u => u.Account).FirstOrDefault(u => u.AccountNumber == AccountNumber);
            if (user == null)
            {
                return true;
            }

            return false;
        }
    }
}
