﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.Models
{
    public class Transaction
    {
        public int Id { get; set; }
        public DateTime TransactionDate { get; set; }
        public int Summ { get; set; }   

        public string UserFormId { get; set; }
        public User UserForm { get; set; }

        public string UserToId { get; set; }
        public User UserTo { get; set; }    
    }
}
